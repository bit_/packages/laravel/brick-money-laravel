<?php

return [
  /**
   * The default currency
   */
  "currency" => "MXN",
  /**
   * The default locale when formatting
   */
  "locale" => "es_MX",
];
