<?php

namespace Bit\BrickMoney;

use Illuminate\Support\ServiceProvider;

class BrickMoneyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/money.php',
            'money'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            \realpath(__DIR__ . '/../config/money.php') => \config_path('money.php'),
        ]);
    }
}
