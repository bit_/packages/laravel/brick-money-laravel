<?php

namespace Bit\BrickMoney\Casts;

use Brick\Money\Money;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class MoneyCast implements CastsAttributes
{
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return \Brick\Money\Money|null
     */
    public function get($model, $key, $value, $attributes): ?Money
    {
        if (is_null($value)) {
            return $value;
        }
        return \makeMoneyFromCents($value);
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model|null  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, $key, $value, $attributes)
    {
        if (is_null($value)) {
            return $value;
        }
        $money =  $value instanceof Money ?
            $value : makeMoneyFromCents($value);
        return \moneyAsCents($money);
    }
}
