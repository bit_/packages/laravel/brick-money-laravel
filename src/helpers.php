<?php

use Brick\Math\BigNumber;
use Brick\Math\Exception\IntegerOverflowException;
use Brick\Math\Exception\MathException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Brick\Money\Exception\UnknownCurrencyException;
use Brick\Money\Money;

if (!function_exists('moneyAsDecimal')) {
  function moneyAsDecimal(Money $money): string
  {
    return (string) $money->getAmount();
  }
}

if (!function_exists('moneyFormatted')) {
  function moneyFormatted(Money $money): string
  {
    return $money->formatTo(\config('money.locale', 'es_MX'));
  }
}

if (!function_exists('moneyAsCents')) {
  /**
   *
   * Return cents from a Money object
   *
   * @param Money $money
   * @return int
   * @throws InvalidArgumentException
   * @throws MathException
   * @throws IntegerOverflowException
   */
  function moneyAsCents(Money $money): int
  {
    return $money->getMinorAmount()->toInt();
  }
}


if (!function_exists('makeMoney')) {
  /**
   *
   * Return a Money object from an amount, ex: `makeMoneyFromDecimal(100.25)`
   *
   * @param float|int|string $amount
   * @return Money
   * @throws NumberFormatException
   * @throws UnknownCurrencyException
   * @throws RoundingNecessaryException
   */
  function makeMoney(float|int|string $amount): Money
  {
    return Money::of($amount, \config('money.currency'));
  }
}

if (!function_exists('makeMoneyFromCents')) {
  /**
   *
   * Returns a Money object from cents value
   *
   * @param int|BigNumber|string $amount
   * @return Money
   * @throws NumberFormatException
   * @throws UnknownCurrencyException
   * @throws RoundingNecessaryException
   */
  function makeMoneyFromCents(int|BigNumber|string $amount): Money
  {
    return Money::ofMinor($amount, \config('money.currency'));
  }
}
