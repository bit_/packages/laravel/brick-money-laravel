# Laravel Brick Money

A money wrapper to use <https://github.com/brick/money> with laravel

## Cast

You ca use to work with money column

## Requirements

- PHP >= 7.3
- laravel >= 8.x

## Installation

### Publish vendor settings

`php artisan vendor:publish --provider="Bit\BrickMoney\BrickMoneyServiceProvider"`

### Default settings

```php
[
    'currency' => 'MXN',
    'locale' => 'es_MX',
]
```

## Use cast on eloquent models

```php
<?php

namespace App;

use Bit\BrickMoney\Casts\MoneyCast;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderItem
 *
 * @property Money $price
 * @property Money $discount
 */
class Product extends Model
{
    ...

    protected $casts = [
        'price' => MoneyCast::class,
    ];
}
```

## Helpers

You can use the follow helpers to display money values

```php

$money = Brick\Money\Money:of(200.65, 'MXN');

moneyFormatted($money); // "$200.65"

moneyAsDecimal($money); // "200.65"

moneyAsCents($money); // 20065

makeMoney(100.25);

makeMoneyFromCents(10025);

```
